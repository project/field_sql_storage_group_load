Field SQL Storage Group Load
----------------------------
Drupal's standard Field storage module loads makes 1 SQL query per field during
entity load, which can be suboptimal with many fields. This module allows loading
multiple fields per query, hence reducing the number of SQL queries during
entity load.

This module only works when loading the current revision of an entity, trying
to load an older revision will still use the Drupal standard way.

Usage
-----
Just install the module, nothing more to do.

By default it will use hook_field_storage_pre_load() to load up to 10 fields
at once. You can change the number of fields by setting the variable
"field_sql_storage_group_load_max_fields".

Note that the resulting query may have a large number of LEFT JOINS, and that
MySQL has a limit in the number of JOINS that can be made in a single query.
Usually this limit is at 61 JOINS.
See: https://dev.mysql.com/doc/refman/5.0/en/joins-limits.html